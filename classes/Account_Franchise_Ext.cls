/*
    Purpose: verify address before saving. Do not allow save of account with invalid address unless override is checked
           designed to work the the Franchise_Edit page
     
    Created By:  Jochen Heyland (Synaptic) 11/21/12
      
    Last Modified By: Jochen Heyland (Synaptic) 7/18/13  
     
    Current Version:  v1.2
     
    Revision Log:   
                v1.0 -   (JH 09/27/12) Created extension
                v1.1 -   (JH 12/18/12) moved address verification to StrikeIron class.
                v1.2 -   (JH 7/16/13) added special check for missing address fields w/o calling Strikeiron.   
                v1.3 -   (AD 7/30/18) added a boolean to know the if the logged-in user is eligible for HSvc_LocationDetail page
                
    */ 
public with sharing class Account_Franchise_Ext 
{
    public ApexPages.StandardController std {set;get;}
    public String billingAddressError {get;set;}
    public String shippingAddressError {get;set;}
    public String cardExpError {get;set;}
    public String PageError{get;set;}
    public String PageError2{get;set;}
    public boolean hasCCerror{get;set;}
    public string params{get;set;}
    public String profileName{get;set;}
    public boolean profilecheck {get;set;}
    public Map<String,ECOB_Profile__c>  profStringMap{get;set;}
    public Boolean isHSvcLocationDetailPageAccessible {get;set;} //v1.3    
    
    public boolean showCCFields{get;set;}
    
    public void showCCFieldsFunction()
    {       
        system.debug('Showing credit card fields');
        showCCFields = true;        
    }
    public void hideCCFieldsFunction()
    {       
        system.debug('hiding credit card fields');
        showCCFields = false;  
        hasCCError = false;         
    }
    public void showCCerror() 
    {
        system.debug('showing credit card number error');
        hasCCerror = true;      
    }
    public void hideCCerror()
    {

        hasCCerror = false; 
    }

    public Account_Franchise_Ext(ApexPages.StandardController stdController)
    {
           isHSvcLocationDetailPageAccessible = HSvc_Service_Cloud_Hierarchical_Util__c.getInstance(UserInfo.getProfileId()).Show_HSvc_LocationDetail_Page__c; //v1.3	
           profStringMap =  ECOB_Profile__c.getAll();
           profilecheck = false;
           profileName = '';
            std = stdController;
          
            for (string s : apexpages.currentpage().getParameters().keyset()) 
                {
                    params += s+'  '+apexpages.currentpage().getParameters().get(s);
                }
            string rt = apexpages.currentpage().getParameters().get('RecordType');
            if ( (rt!=NULL) && (rt!='') )
            {
              account acc =(Account)std.getRecord();
              acc.RecordTypeId=rt;
            }
            Id userId = UserInfo.getUserId();
            profileName = [Select id,Profile.Name from User where Id =: userId LIMIT 1].Profile.Name;
            
            if(profStringMap.containsKey(profileName)){
                
                profilecheck = true;
                
            }else{
                
                profilecheck = false;
            }
    
    }
    
    
    public boolean verifyExp()
    {
        Account acc = (Account)std.getRecord();
        boolean ccExp=true;  // card expiration ok
        pageError2='';
        cardExpError ='';
        if (acc.Payment_Method__c=='Credit Card')
        {
            Date da;
            da = system.today();
            String currentYear = string.valueOf(da.year());
            string currentMonth = string.valueOf(da.month());
            if ( (acc.Expiration_Year__c) >= currentYear )
            {
                if ( acc.Expiration_Year__c == currentYear )
                {
                    if ( acc.Expiration_Month__c < currentMonth )
                    {
                        ccExp = false;
                    }
                }
            }
            else
            {
                ccExp = false;
            }
            if (!ccExp)
            {
                pageError2='Credit card expiration date error';
                cardExpError ='This card is expired.';
            }
        }
        return ccExp;
    }
    
    public boolean verifyAddresses()
    {
        boolean BillingAddrOk,ShippingAddrOk = false;
        Account acc = (Account)std.getRecord();
        StrikeIron.Address addrToVerify = new StrikeIron.Address(); 
        
        
        if ( (acc.BillingStreet != NULL) || (acc.billingState != NULL) || (acc.BillingPostalCode != NULL) || 
             (acc.BillingCity != NULL) || (acc.BillingPostalCode != NULL) )
        {
                boolean missingFields = false;
                if (acc.BillingStreet == NULL)
                {
                        acc.BillingStreet.AddError('A street address is required for addresses');
                        missingFields = true;
                }
                if (acc.BillingCity == NULL)
                {
                        acc.BillingCity.AddError('City is required for addresses');
                        missingFields = true;
                }
                if (acc.BillingState == NULL)
                {
                        acc.BillingState.AddError('State is required for addresses');
                        missingFields = true;
                }
                if (acc.BillingPostalCode == NULL)
                {
                        acc.BillingPostalCode.AddError('Zipcode is required for addresses');
                        missingFields = true;
                }
                if (missingFields)
                        return false; // =========================================>
        }
                if ( (acc.shippingStreet != NULL) || (acc.shippingState != NULL) || (acc.shippingPostalCode != NULL) || 
             (acc.shippingCity != NULL) || (acc.shippingPostalCode != NULL) )
        {
                boolean missingFields = false;
                if (acc.shippingStreet == NULL)
                {
                        acc.shippingStreet.AddError('A street address is required for addresses');
                        missingFields = true;
                }
                if (acc.shippingCity == NULL)
                {
                        acc.shippingCity.AddError('City is required for addresses');
                        missingFields = true;
                }
                if (acc.shippingState == NULL)
                {
                        acc.shippingState.AddError('State is required for addresses');
                        missingFields = true;
                }
                if (acc.shippingPostalCode == NULL)
                {
                        acc.shippingPostalCode.AddError('Zipcode is required for addresses');
                        missingFields = true;
                }
                if (missingFields)
                        return false; // =========================================>
        }        
        
        billingAddressError='';
        shippingAddressError='';
        
        List<Account> AconList = new List<Account>();
                AconList.add(acc);
                StrikeIron.AccountAddressStatus status = StrikeIron.verifyAccount(AconList); 
                        
                billingAddressError = status.billingAddressError;
                shippingAddressError = status.shippingAddressError;

        if (!status.accountAddrGood())
            PageError = 'An error occured during the address verification, please see below for details';
        return status.accountAddrGood();
        
    }
    public PageReference  VerifyAndSave()
    {
        boolean okExp =verifyExp();
        boolean okAddr = verifyAddresses(); 
        if (okExp && okAddr)
        {   
            system.debug('Saving');
            return std.save();
        }
        else
            return NULL;
    }  
	
	@AuraEnabled
    public static String franchiseEditMethod(Id accId, Id recordTypeId)
    {
        boolean profilecheck ;
        Map<String,ECOB_Profile__c>  profStringMap;
        profStringMap =  ECOB_Profile__c.getAll();
        profilecheck = false;
        Id userId = UserInfo.getUserId();
        String profileName = [Select id,Profile.Name from User where Id =: userId LIMIT 1].Profile.Name;
        String finalUrl=null;
        if(profStringMap.containsKey(profileName))
        {
            profilecheck = true;
        }
        if(String.isBlank(accId))
        {
            if(recordTypeId == Label.ECOB_AccountLocationRecordTypeId && profilecheck)
            {
                finalURL = '/apex/ECOB_LocAccCreateEditPage?RecordType='+recordTypeId+'&ent=Account&nooverride=1';
            }
            else
            {
                finalURL = 'New';
 
            }
        }
        else
        {
            Account acc = [select recordtypeid from Account where id=:accid];
            recordTypeId = acc.recordtypeid;
            
            System.debug('recordTypeId-->'+ recordTypeId);
                      
            if(recordTypeId == Label.ECOB_AccountLocationRecordTypeId_18 && profilecheck)
            {
                finalURL = '/apex/ECOB_LocAccCreateEditPage?Id='+accId;
            }
            else
            {
                Boolean isHSvcLocationDetailPageAccessible = HSvc_Service_Cloud_Hierarchical_Util__c.getInstance(UserInfo.getProfileId()).Show_HSvc_LocationDetail_Page__c; //v1.3
                if(isHSvcLocationDetailPageAccessible)
                {
                    finalURL = '/apex/HSvc_LocationDetail?Id='+accid;
                }
                else
                {

                    finalURL = 'edit?nooverride=1';
                }
            }
        }
        System.debug('URL being sent: ' + finalURL);
        return finalURL;
    }

}
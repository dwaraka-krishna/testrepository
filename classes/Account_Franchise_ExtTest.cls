@isTest
private class Account_Franchise_ExtTest {
    static Id accRT;
    static  Account acct;
    static String currentYear;
    static String previousYear;
    static String previousMonth;
    static String currentMonth;
    static {
         // Create StrikeIron Configuration object with Username and Password
        StrikeForce1__USAddressConfiguration__c strikeIronConfig = new StrikeForce1__USAddressConfiguration__c(
                                                                    StrikeForce1__UserID__c='testusername',
                                                                    StrikeForce1__Password__c='testpassword');
        insert strikeIronConfig;
        
        accRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.ECOB_Account_Developer_Name_Franchise).getRecordTypeId();
        
        Date da; 
        da = system.today();
        currentYear = string.valueOf(da.year());
        previousYear = string.valueOf(da.year()-1);        
        currentMonth = string.valueOf(da.month());        
        previousMonth = string.valueOf(da.month()-1);       
        
       
    }
    
  
    static testMethod void unitTest1() {
        acct = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Month__c=currentMonth,
        Expiration_Year__c=currentYear,
        billingStreet = '182 Covert Ave',
        billingState ='NY',
        billingPostalCode ='11001',
        billingCity='Floral Park',
        shippingStreet = '11717 Exploration Ln',
        shippingState ='MD',
        shippingPostalCode ='20876',
        shippingCity='Germantown'
        );
                     
        insert acct;
        PageReference pageRef = new PageReference('http://www.google.com');
        
        //create account good values
       
        Test.setCurrentPageReference(pageRef);
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new StrikeIronHttpMockImpl());
        ApexPages.CurrentPage().getparameters().put('RecordType', accRT); // Franchise Record Type SFDC Id
        ApexPages.StandardController sc = new ApexPages.standardController(acct);
        Account_Franchise_Ext acctFranchiseExt = new Account_Franchise_Ext(sc);
        
        acctFranchiseExt.showCCFieldsFunction();
        acctFranchiseExt.hideCCFieldsFunction();
        acctFranchiseExt.showCCerror(); 
        acctFranchiseExt.hideCCerror();
        acctFranchiseExt.VerifyAndSave();
        System.assertEquals(acctFranchiseExt.verifyAddresses(),true);
         
        test.stopTest();       
        
         
    }
    
   
    static testMethod void unitTest2() {
        acct = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Year__c = previousYear,
        Expiration_Month__c = previousMonth,
        billingStreet = '182 Covert Ave',
        billingState ='NY',
        billingPostalCode ='11001',
        billingCity='Floral Park',
        shippingStreet = '11717 Exploration Ln',
        shippingState ='MD',
        shippingPostalCode ='20876',
        shippingCity='Germantown'
        );
                     
      
        PageReference pageRef = new PageReference('http://www.google.com');
        
        //create account good values
       
        Test.setCurrentPageReference(pageRef);
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new StrikeIronHttpMockImpl());
        ApexPages.CurrentPage().getparameters().put('RecordType', accRT); // Franchise Record Type SFDC Id
        ApexPages.StandardController sc = new ApexPages.standardController(acct);
        Account_Franchise_Ext acctFranchiseExt = new Account_Franchise_Ext(sc);
        
         
         //verifyExp() : Good CC
         System.assertEquals(acctFranchiseExt.verifyExp(),false);
           //verifyAddresses()
        // acctFranchiseExt.verifyAddresses();
         acctFranchiseExt.VerifyAndSave();
         
         
         acct = new Account(
         name='Acct2',
         Account_Customer_ID__c='1236',
         Payment_Method__c = 'Credit Card',
         Credit_Card_Number__c='4111111111111111',
         Expiration_Year__c = currentYear,
         Expiration_Month__c = previousMonth,
         billingStreet = '182 Covert Ave',
         billingState ='NY',
         billingPostalCode ='11001',
         billingCity='Floral Park',
         shippingStreet = '11717 Exploration Ln',
         shippingState ='MD',
         shippingPostalCode ='20876',
         shippingCity='Germantown'
         );
         sc = new ApexPages.standardController(acct);
         acctFranchiseExt = new Account_Franchise_Ext(sc);
         acctFranchiseExt.VerifyAndSave();
         test.stopTest();       
        
         
    }

    static testMethod void unitTest3() {
        acct = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Year__c = currentYear,
        Expiration_Month__c = previousMonth,
        billingStreet = '182 Covert Ave',
        //billingState ='NY',
        //billingPostalCode ='11001',
       // billingCity='Floral Park',
        shippingStreet = '11717 Exploration Ln',
        shippingState ='MD',
        shippingPostalCode ='20876',
        shippingCity='Germantown'
        );
     
        PageReference pageRef = new PageReference('http://www.google.com');
        //create account good values
        Test.setCurrentPageReference(pageRef);
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class,new StrikeIronHttpMockImpl());
        ApexPages.CurrentPage().getparameters().put('RecordType', accRT); // Franchise Record Type SFDC Id
        ApexPages.StandardController sc = new ApexPages.standardController(acct);
        Account_Franchise_Ext acctFranchiseExt = new Account_Franchise_Ext(sc);         
        acctFranchiseExt.VerifyAndSave();
        
        
        
        //2.
        acct = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Year__c = currentYear,
        Expiration_Month__c = currentMonth,
        //billingStreet = '182 Covert Ave',
        billingState ='NY',
        //billingPostalCode ='11001',
       // billingCity='Floral Park',
        shippingStreet = '11717 Exploration Ln',
        shippingState ='MD',
        shippingPostalCode ='20876',
        shippingCity='Germantown'
        );
        sc = new ApexPages.standardController(acct);
        acctFranchiseExt = new Account_Franchise_Ext(sc);         
        acctFranchiseExt.VerifyAndSave();
        
        //3.
        acct = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Year__c = currentYear,
        Expiration_Month__c = currentMonth,
        billingStreet = '182 Covert Ave',
        billingState ='NY',
        billingPostalCode ='11001',
        billingCity='Floral Park',
        shippingStreet = '11717 Exploration Ln'
        //shippingState ='MD',
        //shippingPostalCode ='20876',
        //shippingCity='Germantown'
        );
        sc = new ApexPages.standardController(acct);
        acctFranchiseExt = new Account_Franchise_Ext(sc);         
        acctFranchiseExt.VerifyAndSave();
        
        //4.
        acct = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Year__c = currentYear,
        Expiration_Month__c = currentMonth,
        billingStreet = '182 Covert Ave',
        billingState ='NY',
        billingPostalCode ='11001',
        billingCity='Floral Park',
        //shippingStreet = '11717 Exploration Ln',
        shippingState ='MD'
        //shippingPostalCode ='20876',
        //shippingCity='Germantown'
        );
        sc = new ApexPages.standardController(acct);
        acctFranchiseExt = new Account_Franchise_Ext(sc);         
        acctFranchiseExt.VerifyAndSave();
        
        System.assertEquals(acctFranchiseExt.verifyAddresses(),false);
        
        //acctFranchiseExt.Redirect();
            
        
        test.stopTest();       
        
         
    }
    static testMethod void unitTest4() {
        
        test.StartTest();
        Account accnew = new account();
        ApexPages.StandardController sc1111 = new ApexPages.standardController(accnew);
        Account_Franchise_Ext Acc = new Account_Franchise_Ext(sc1111);
        //Acc.Redirect();
        
        try{
        ApexPages.CurrentPage().getparameters().put('RecordType', '234'); // Franchise Record Type SFDC Id
        ApexPages.CurrentPage().getparameters().put('Id', '234678');
        ApexPages.StandardController sc11 = new ApexPages.standardController(accnew);
        Account_Franchise_Ext acctFranchiseExt = new Account_Franchise_Ext(sc11);
        //acctFranchiseExt.Redirect();
        }catch(exception e){}
        Try{
        //ApexPages.CurrentPage().getparameters().put('Acccid', null); // Franchise Record Type SFDC Id
        ApexPages.StandardController sc111 = new ApexPages.standardController(accnew);
        Account_Franchise_Ext acctFranchiseExt11 = new Account_Franchise_Ext(sc111);
        //acctFranchiseExt11.Redirect();
        }Catch(exception e){}
        Account acct1 = new Account(
        name='Acct2',
        Account_Customer_ID__c='1236',
        Payment_Method__c = 'Credit Card',
        Credit_Card_Number__c='4111111111111111',
        Expiration_Year__c = currentYear,
        Expiration_Month__c = currentMonth,
        billingStreet = '182 Covert Ave',
        billingState ='NY',
        billingPostalCode ='11001',
        billingCity='Floral Park',
        //shippingStreet = '11717 Exploration Ln',
        shippingState ='MD'
        //shippingPostalCode ='20876',
        //shippingCity='Germantown'
        );
        
        Insert acct1;
        Try{
        //String AccId = acct1.id;
        //ApexPages.CurrentPage().getparameters().put('Id', AccId); // Franchise Record Type SFDC Id
        ApexPages.StandardController sc1111111 = new ApexPages.standardController(acct1);
        Account_Franchise_Ext acctFranchiseExt1111 = new Account_Franchise_Ext(sc1111111);
        //acctFranchiseExt1111.Redirect();
        
        //ApexPages.CurrentPage().getparameters().put('Id', AccId); // Franchise Record Type SFDC Id
        ApexPages.StandardController sc11111111 = new ApexPages.standardController(accnew);
        Account_Franchise_Ext acctFranchiseExt11111 = new Account_Franchise_Ext(sc11111111);
        //acctFranchiseExt11111.Redirect();
        }Catch(exception e){}
        
        Id FranchiseRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.ECOB_Account_Developer_Name_Franchise).getRecordTypeId();
        Id BrandId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.ECOB_Account_Developer_Name_BRAND).getRecordTypeId();
        
        Account acct123 = new Account(name='Acct3', RecordtypeId=BrandId, Program_Code__c='123');
        Insert acct123;
        
        Account acct1234 = new Account(name='Acct4', RecordtypeId=FranchiseRecordTypeId, ParentId=acct123.Id);
        Insert acct1234;
        
        Update acct1234;
        
        test.stopTest();       
              
    }
}